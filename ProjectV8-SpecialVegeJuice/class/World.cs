﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectV8_SpecialVegeJuice
{
    class World
    {
        private Timer timer;
        public List<Villager> villagers { get; }

        const int startingVillagers = 1;
        public const int width = 10;
        public const int height = 10;

        public World()
        {
            villagers = new List<Villager>();

            timer = new System.Windows.Forms.Timer();
            timer.Interval = 1000;
            timer.Enabled = true;
            timer.Tick += new System.EventHandler(this.TimerTick);
        }

        public void Init()
        {
            // Add initial villagers
            for (int i = 0; i < startingVillagers; i++)
            {
                villagers.Add(new Villager());
            }
        }

        /// <summary>
        /// Base timer for all the world components
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerTick(object sender, EventArgs e)
        {
            Update();
            //Render();
        }

        private void Render()
        {
            // TODO : this is where visual map will be updated
            throw new NotImplementedException();
        }

        /// <summary>
        /// Single tick update
        /// </summary>
        private void Update()
        {
            foreach(Villager villager in this.villagers)
            {
                villager.Update();
            }

            // TODO : update buildings here
            // TODO : update tiles here
        }
    }
}
