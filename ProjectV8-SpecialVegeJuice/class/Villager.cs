﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectV8_SpecialVegeJuice
{
    class Villager
    {
        const int maxFood = 72; // If you don't eat in 72 hours, you die.
        const int maxEnergy = 48; // If you don't sleep for 48 hours, you die.

        private int physic;
        private int mental;
        private int social;
        private int food = maxFood;
        private int energy = maxEnergy;
        private bool alive = true;
        private string action = null;

        public Location location { get; }

        public Villager()
        {
            Random rnd = new Random();

            // Init random stats
            physic = rnd.Next(0, 5);
            mental = rnd.Next(0, 5);
            social = rnd.Next(0, 5);

            location = new Location(4, 3);
        }

        /// <summary>
        /// Single tick update
        /// </summary>
        public void Update()
        {
            if (!alive)
            {
                return;
            }

            food--;
            energy--;

            if (food <= 0 || energy <= 0)
            {
                Death();
                return;
            }

            DoSomething();
        }

        private void DoSomething()
        {

            Random rnd = new Random();
            int randomAction = rnd.Next(1, 100);

            if (action != null)
            {
                if (food <= maxFood - 5)
                {
                    // Eat every 5 hours
                    action = "findFood";
                }
                else if (energy <= maxEnergy - 16)
                {
                    // Need to sleep after being awake for 16 hours
                    action = "sleep";
                }
            }

            switch (action)
            {
                case "findFood":
                    // Move to tile with food
                    break;

                case "eat":
                    // If on tile with food, eat "Food", no inventory yet

                    // Eating just makes you go back to 100% for now
                    food = maxFood;
                    break;

                case "sleep":
                    energy += 2;

                    // Stop sleeping once full
                    if (energy >= maxEnergy)
                    {
                        energy = maxEnergy;
                        action = null;
                    }
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Death()
        {
            alive = false;
        }
    }
}
