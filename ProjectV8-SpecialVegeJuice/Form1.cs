﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectV8_SpecialVegeJuice
{
    public partial class Form1 : Form
    {


        public Bitmap mapTile;
        public static List<Bitmap> tilesBitmap;
        public static List<Bitmap> citizenBitmap;
        public static List<Bitmap> buildingBitmap;
        public static int totalTick = 0;

        World world;

        public Form1()
        {
            InitializeComponent();
            loadImages();

            world = new World();
            world.Init();
        }

        private void loadImages()
        {
            Regex regex = new Regex(@"Image\.(.+)\.([0-9]{3})");

            string[] tileImages = System.Reflection.Assembly.GetEntryAssembly().GetManifestResourceNames();
            Array.Sort(tileImages);

            tilesBitmap = new List<Bitmap>();
            citizenBitmap = new List<Bitmap>();
            buildingBitmap = new List<Bitmap>();
            foreach (string tileImage in tileImages)
            {
                Match match = regex.Match(tileImage);
                if (match.Success)
                {
                    switch (match.Groups[1].Value)
                    {
                        case "Tile":
                        case "Citizen":
                        case "Building":

                            Bitmap bmp = new Bitmap(
                                System.Reflection.Assembly.GetEntryAssembly().
                                GetManifestResourceStream(tileImage));

                            if (match.Groups[1].Value == "Tile")
                            {
                                tilesBitmap.Add(ResizeBitmap(bmp, 32, 32));
                            }
                            else if (match.Groups[1].Value == "Citizen")
                            {
                                citizenBitmap.Add(ResizeBitmap(bmp, 32, 32));
                            }
                            else if (match.Groups[1].Value == "Building")
                            {
                                buildingBitmap.Add(ResizeBitmap(bmp, 32, 32));
                            }
                            break;
                    }

                    //logs.addLog("Loading tile image " + tileImage, "loading");

                }
            }
        }

        public Bitmap ResizeBitmap(Bitmap b, int nWidth, int nHeight)
        {
            Bitmap result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
                g.DrawImage(b, 0, 0, nWidth, nHeight);

            return result;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gfx = e.Graphics;

            if (true)
            {
                //int y = 0;
                for (int x = 0; x < World.width; x++)
                {
                    for (int y = 0; y < World.height; y++)
                    {

                        Random rnd = new Random();
                        gfx.DrawImage(tilesBitmap[rnd.Next(1, 4)], x * 32, y * 32);
                        //if (block.citizenIds.Count > 0)
                        //{
                        //	gfx.DrawImage(citizenImages[1], x * 32, y * 32);
                        //}
                    }
                }

                foreach(Villager villager in world.villagers)
                {
                    gfx.DrawImage(citizenBitmap[1], villager.location.y * 32, villager.location.x * 32);
                    
                }
                //foreach (List<Block> row in world.map.blocks)
                //{
                //	int x = 0;
                //	foreach (Block block in row)
                //	{

                //		gfx.DrawImage(tilesImages[block.blockType], x * 32, y * 32);
                //		if (block.citizenIds.Count > 0)
                //		{
                //			gfx.DrawImage(citizenImages[1], x * 32, y * 32);
                //		}
                //		x++;
                //	}
                //	y++;
                //}
            }
        }
    }
}
