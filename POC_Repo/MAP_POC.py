# import player as p

# player = p.player("Bob")

# print(player.name)


import math
import random as rand
import os
import time
# import keyboard

## Plains = 0
## Forest = 1
## Water = 2
## Mountain = 3


class bcolor:
    FOREST = '\033[42m'
    PLAINS = '\033[105m'
    WATER = '\033[104m'
    MOUNTAIN = '\033[41m'
    PLAYER = '\033[70m'
    DEFAULT = '\033[0m'


class loaded_map:
    rows = None
    h = 4
    w = 4
    x = 0
    y = 0

    def __init__(self, h, w, x, y):
        self.h = h
        self.w = w
        self.x = x
        self.y = y
        
        self.rows = []
        # Build initial map storage
        for i in range(self.h):
            self.rows.append([])
            for j in range(self.w):
                self.rows[i].append(None)
                                
        self.fill()
                

    def move(self, d, amount=1):
        #print("=====")
        #print(self.rows)
        for i in range(amount):
            if d == "u":
                self.y += 1
                self.__removeBottomRow()
                self.__addTopRow()
            elif d == "r":
                self.x +=1
                self.__removeLeftCol()
                self.__addRightCol()
            elif d == "d":
                self.y -= 1
                self.__removeTopRow()
                self.__addBottomRow()
            elif d == "l":
                self.x -= 1
                self.__removeRightCol()
                self.__addLeftCol()
        #print(self.rows)
        # Rebuild the missing tiles
        self.fill()
        #print(self.rows)
        #quit()


    def fill(self):
        print(self.rows[19][29])
        for j in range(self.h):
            for i in range(self.w):
                x = int(i - self.w/2 + self.x)
                y = int(j - self.h/2 + self.y)
                #print("Checking "+str(i)+","+str(j)+" Real coord "+str(x)+","+str(y))
                if self.rows[j][i] is None:
                    # Translate i,j into x,y to be
                    
                    #print("Loading "+str(i)+","+str(j)+" Real coord"+str(x)+","+str(y))
                    self.rows[j][i] = map_v2(x,y)
            

    def __addLeftCol(self):
        for row in self.rows:
            row.insert(0, None)

    def __addRightCol(self):
        for row in self.rows:
            row.append(None)

    def __addTopRow(self):
        self.rows.insert(0, [])
        for j in range(self.w):
            self.rows[0].append(None)

    def __addBottomRow(self):
        self.rows.append([])
        for j in range(self.w):
            self.rows[self.h-1].append(None)

    def __removeLeftCol(self):
        for row in self.rows:
            row.pop(0)

    def __removeRightCol(self):
        for row in self.rows:
            row.pop()

    def __removeTopRow(self):
        self.rows.pop(0)

    def __removeBottomRow(self):
        self.rows.pop()

    def getTile(self):
        pass


def print_map(myMap, debug=False):    
    def print_square(val):
        if val == 0:
            return bcolor.PLAINS + 'P' + bcolor.DEFAULT
        elif val == 1:
            return bcolor.FOREST + 'F' + bcolor.DEFAULT
        elif val == 2:
            return bcolor.WATER + 'W' + bcolor.DEFAULT
        elif val == 3:
            return bcolor.MOUNTAIN + 'M' + bcolor.DEFAULT
        elif val == 4:
            return bcolor.PLAYER + '@' + bcolor.DEFAULT
        else:
            return bcolor.DEFAULT + 'Z' + bcolor.DEFAULT

    g_map = ''

    for j in range(myMap.h):
        for i in range(myMap.w):
            if int(myMap.w/2) == i and int(myMap.h/2) == j:
                val = 4
            else:
                val = myMap.rows[j][i]
            g_map += print_square(val)
        g_map += bcolor.DEFAULT+'\n'
        
    if debug:
        g_map = '\n'.join([''.join(['{:1}'.format(item) for item in row]) for row in myMap.rows])
    
    return g_map


def map_v1(x, y):
    if x == y:
        return 1
    elif x == 0 or y == 0:
        return 2
    else:
        return 0


def map_v2(x, y):
    rand.seed(cantor_pairing(x, y))
    return rand.randint(0, 3)


def map_v3(x, y):

    pass


def naturalize_int(z):
    z = 2 * z if z >= 0 else -2 * z - 1
    return z


def cantor_pairing(x, y):
    x = naturalize_int(x)
    y = naturalize_int(y)
    return int((1.0/2.0)*(x+y)*(x+y+1)+y)


def main():
    print(cantor_pairing(200, 100))
    print(cantor_pairing(27, 42))
    print(cantor_pairing(1, 73))
    print(cantor_pairing(0, 0))
    print(cantor_pairing(-27, -42))
    print(cantor_pairing(73, 1))
    print(cantor_pairing(0, 100))

    x = 0
    y = 0
    w = 200  # 200
    h = 120  # 128
    #map = [['z' for i in range(h+1)] for j in range(h+1)]

    myMap = loaded_map(h, w, x, y)        
    while True:        
        myMap.move("u")
        g_map = print_map(myMap)
        
        time.sleep(0.15)
        os.system('clear')
        print(g_map)
        
    #input('waiting')


main()
